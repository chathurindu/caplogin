from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label=False, widget=forms.TextInput(
                                                            attrs={
                                                                'class': 'form-control mb-3',
                                                                'autofocus': 'autofocus',
                                                            }
                                                    ))
    password = forms.CharField(label=False,widget=forms.PasswordInput(
                                                            attrs={
                                                                'class': 'form-control',
                                                                'autofocus': 'autofocus',
                                                            }
                                                    ))
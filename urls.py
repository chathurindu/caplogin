from django.urls import path
from caplogin.views import *

urlpatterns = [
    path('',login_page, name="login"),
    path('logout/',log_out,name='logout'),
]

from django.db.models import Q
from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout


def login_page(request):
    if request.user.is_authenticated:
        if request.user.groups.filter(Q(name="Team Member") | Q(name="Manager")).exists():
            return redirect('ProjectList')
        else:
            return render(request, "caplogin/warning.html")
    form = LoginForm(request.POST or None)
    error = ' '
    print(request.user.is_authenticated)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            print("login success")
            if request.user.groups.filter(Q(name="Team Member") | Q(name="Manager")).exists():
                return redirect('ProjectList')
            else:
                return render(request, "caplogin/warning.html")
        else:
            error = form

    context = {
        'form': form,
        'error': error,
    }
    return render(request, 'caplogin/login.html', context)


def log_out(request):
    logout(request)
    return redirect('login')


def notfound(request, exception, template_name="errors/404.html"):
    response = render('errors/400.html')
    response.status_code = 404 
    return response

def error_500_view(request):
    return render(request,'errors/500.html')
